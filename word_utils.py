def remove_punctuation(text):
    new_text = ''
    for ch in text:
        if ch not in '!?.,:;':
            new_text += ch
    return new_text


def word_list(text):
    new_text = remove_punctuation(text)
    return new_text.split()


def longest_word(text):
    long_word = ''
    lst = word_list(text)
    for word in lst:
        if len(word) > len(long_word):
            long_word = word
    return long_word
