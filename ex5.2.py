import word_utils as wd


text = 'Lorem ipsum dolor sit amet,' \
       ' consectetur adipiscing elit,' \
       ' sed do eiusmod tempor incididunt ' \
       'ut labore et dolore magna aliqua.'

print(wd.remove_punctuation(text))
print(wd.word_list(text))
print(wd.longest_word(text))
