original_file = input('Введите название файла исходника: ')

try:
    with open(original_file) as file:
        lst = [f'{str(ind+1)}: {val}' for ind, val in enumerate(file)]
except FileNotFoundError:
    print('File Not Found')
    raise 'Проверьте правилность ввода! Файл должен быть в этом каталоге!'

target_file = input('Введите название целевого файл: ')

try:
    with open(target_file, 'w') as file_2:
        file_2.writelines(lst)
except Exception:
    print('ERROR')
